<?php

function gls_dc_get_data_store($store)
{
    $out = gls_dc_get_map_store($store['Latitude'], $store['Longitude']);
    $out .= $store['CompanyName'] . '<br>';
    $out .= $store['ZipCode'] . '<br>';
    $out .= $store['CityName'] . '<br>';
    $out .= $store['Streetname'] . ' ' . $store['Streetname2'][0] . '<br>';
    $out .= $store['Telephone'];
    $out .= gls_dc_get_table_worktime($store['OpeningHours']);
    return array('#markup' => $out,);
}

function gls_dc_get_table_worktime($data)
{
    $out = '';
    if (isset($data['Weekday'])) {
        if (count($data['Weekday']) > 0) {
            $header = array('Day', 'Open', 'Breaks');
            $rows = array();

            foreach ($data['Weekday'] as $wday) {
                $rows[] = array($wday['day'], $wday['openAt']['From'] . ' - ' . $wday['openAt']['To'], $wday['breaks']['From'] . ' - ' . $wday['breaks']['To']);
            }

            $out = theme('table', array('caption' => t('Working hours'), 'header' => $header, 'rows' => $rows));
        }
    }
    return $out;

}

function gls_dc_get_map_store($latitude, $longitude)
{
    if (variable_get('gls_dc_gmshow', 0) == 1) {
        global $user;
        $mess = t("Display the geographical position is impossible. Notify administrator ");
        if (in_array('administrator', $user->roles)) $mess = t("Please indicate the Google Maps KEY of the ") . l(t('settings'), 'admin/commerce/config/shipping/gls');

        if (strlen(trim(variable_get('gls_dc_gmkey', ''))) > 0)
            return '<iframe width="' . variable_get('gls_dc_gm_width', 600) . '" height="' . variable_get('gls_dc_gm_height', 450) . '" frameborder="0" style="border:0"  src="https://www.google.com/maps/embed/v1/place?q=' . $latitude . '%2C' . $longitude . '%20&key=' . variable_get('gls_dc_gmkey', '') . '" allowfullscreen></iframe>';
        else
            return '<span style="border: 1px solid red;color: red;float: left;margin: 5px;padding: 10px;width: 100%;">' . $mess . '</span>';
    } else
        return '';
}

function gls_dc_get_list_stores($zipcode, $country)
{
    $arr_out = array();
    $data = array(
        'zipcode' => $zipcode,
        'countryIso3166A2' => $country
    );
    $result = drupal_http_request('http://www.gls.dk/webservices_v4/wsShopFinder.asmx/GetParcelShopsInZipcode', array(
        'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
        'method' => 'POST',
        'data' => http_build_query($data),
    ));

    if ($result->status_message == 'OK') {
        $data = $result->data;
        $in_data = simplexml_load_string($data);
        $arr_out = json_decode(json_encode($in_data), TRUE);
        $arr_out = $arr_out['PakkeshopData'];
    }
    return $arr_out;
}

/*
* admin/commerce/customer-profiles/types/shipping/fields/commerce_customer_address
* Organisation (single line)
*/

function gls_dc_form_alter(&$form, &$form_state, $form_id)
{
    if ($form['#form_id'] == 'commerce_checkout_form_review') {
        $shipping_line = gls_dc_get_shipping_line($form_state['order']->commerce_line_items[LANGUAGE_NONE]);
        if (count($shipping_line) > 0 && isset($shipping_line['shipping_service']) && isset($shipping_line['shipping_service']['name']) && $shipping_line['shipping_service']['name'] == 'gls_shipping_service_store') {
            $tmp = unserialize($shipping_line['service_details']['stores']['arrstore']);
            $store = $tmp[$shipping_line['service_details']['stores']['store']];

            $form['checkout_review']['review']['#data']['customer_profile_shipping']['data'] =
                $form['checkout_review']['review']['#data']['customer_profile_shipping']['data'] .
                ' <strong>After confirmation, the shipping address will be replaced by the address of the GLS store:</strong><br>'
                . $store['CountryCodeISO3166A2'] . ', ' . $store['ZipCode'] . ', ' . $store['CityName']
                . '<br>' . $store['CompanyName'] . '<br>' . $store['Streetname'] . ' ' . $store['Streetname2'];
            $form['buttons']['continue']['#submit'][] = 'gls_dc_review_add';
        }
    }
}

function gls_dc_get_shipping_line($in)
{
    $data = NULL;
    $ar_in = array();
    foreach ($in as $line) {
        $ar_in[] = $line['line_item_id'];
    }
    if (count($ar_in) > 0) {
        $data = db_select('commerce_line_item', 'l')
            ->fields('l', array('data'))
            ->condition('l.line_item_id', $ar_in, 'IN')
            ->condition('l.type', 'shipping')
            ->execute()
            ->fetchField();
    }
    return unserialize($data);

}

function gls_dc_get_new_profile($stores_in, $uid)
{
    $arrstore = unserialize($stores_in['arrstore']);
    $store = $arrstore[$stores_in['store']];
    $type = 'shipping';
    $profile = commerce_customer_profile_new($type, $uid);
    $defaults = array();
    $defaults['name_line'] = $type;
    $field = field_info_field('commerce_customer_address');
    $instance = field_info_instance('commerce_customer_profile', 'commerce_customer_address', $type);
    $values = array_merge($defaults, addressfield_default_values($field, $instance));
    $values['organisation_name'] = $store['CompanyName'];
    $values['postal_code'] = $store['ZipCode'];
    $values['thoroughfare'] = $store['Streetname'] . ' ' . $store['Streetname2'];
    $values['locality'] = $store['CityName'];
    $values['country'] = $store['CountryCodeISO3166A2'];

    $profile->commerce_customer_address[LANGUAGE_NONE][] = $values;
    commerce_customer_profile_save($profile);
    return $profile;
}

function gls_dc_review_add($form, &$form_state)
{
    $order = commerce_order_load($form_state['order']->order_id);
    $shipping_line = gls_dc_get_shipping_line($order->commerce_line_items[LANGUAGE_NONE]);
    if (count($shipping_line) > 0 && isset($shipping_line['shipping_service']) && isset($shipping_line['shipping_service']['name']) && $shipping_line['shipping_service']['name'] == 'gls_shipping_service_store') {
        $profile = gls_dc_get_new_profile($shipping_line['service_details']['stores'], $order->uid);
        $profile_old = commerce_customer_profile_load($order->commerce_customer_shipping[LANGUAGE_NONE][0]['profile_id']);
        $order->commerce_customer_shipping[LANGUAGE_NONE][0]['profile_id'] = $profile->profile_id;
        commerce_order_save($order);
        if (commerce_customer_profile_can_delete($profile_old)) commerce_customer_profile_delete($profile_old->profile_id);
    }
}