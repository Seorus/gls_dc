<?php


function gls_dc_admin_settings()
{
    $form['price'] = array(
        '#type' => 'fieldset',
        '#title' => t('Prices'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );
    $form['price']['price_private'] = array(
        '#type' => 'textfield',
        '#title' => t('Home delivery (Private) price [default]'),
        '#default_value' => variable_get('gls_dc_price_private', 0),
        '#size' => 10,
        '#field_prefix' => '$'
    );

    $form['price']['price_companies'] = array(
        '#type' => 'textfield',
        '#title' => t('Company delivery (Companies) price [default]'),
        '#default_value' => variable_get('gls_dc_price_companies', 0),
        '#size' => 10,
        '#field_prefix' => '$'
    );
    $form['price']['table'] = array(
        '#type' => 'fieldset',
        '#title' => t('list prices'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );

    $form['price']['table']['tbl'] = array(
        '#markup' => gls_dc_admin_get_table(),
    );
    $form['price']['table']['operation'] = array(
        '#type' => 'fieldset',
        '#title' => t('With the price of the operation'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );
    $form['price']['table']['operation']['price_op_country'] = array(
        '#type' => 'textfield',
        '#title' => t('Country'),
        '#default_value' => '',
        '#size' => 2,
    );
    $form['price']['table']['operation']['price_op_private'] = array(
        '#type' => 'textfield',
        '#title' => t('Home delivery (Private) price'),
        '#default_value' => 0,
        '#size' => 10,
        '#field_prefix' => '$'
    );
    $form['price']['table']['operation']['price_op_companies'] = array(
        '#type' => 'textfield',
        '#title' => t('Company delivery (Companies) price'),
        '#default_value' => 0,
        '#size' => 10,
        '#field_prefix' => '$'
    );

    $form['price']['table']['operation']['price_op_companies'] = array(
        '#type' => 'textfield',
        '#title' => t('Company delivery (Companies) price'),
        '#default_value' => 0,
        '#size' => 10,
        '#field_prefix' => '$'
    );
    $form['price']['table']['operation']['info'] = array(
        '#markup' => '<div style="float:left; width: 100%"> ' . t('To remove - enter the country code, price, leave empty (remove the zero)<br>To edit - enter the country code and the new prices') . '</div>',

    );

    $form['price']['table']['operation']['operation'] = array(
        '#type' => 'submit',
        '#value' => t('Ok'), // Title to submit verify !!!
    );


    $form['gmap'] = array(
        '#type' => 'fieldset',
        '#title' => t('Google Maps setting'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );
    $form['gmap']['gmshow'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show map'),
        '#default_value' => variable_get('gls_dc_gmshow', 0),
        '#size' => 100,
    );
    $form['gmap']['gmkey'] = array(
        '#type' => 'textfield',
        '#title' => t('Google Maps KEY'),
        '#default_value' => variable_get('gls_dc_gmkey', ''),
        '#size' => 100,
    );
    $form['gmap']['gm_width'] = array(
        '#type' => 'textfield',
        '#title' => t('Google Maps width'),
        '#default_value' => variable_get('gls_dc_gm_width', 600),
        '#size' => 4,
    );
    $form['gmap']['gm_height'] = array(
        '#type' => 'textfield',
        '#title' => t('Google Maps height'),
        '#default_value' => variable_get('gls_dc_gm_height', 450),
        '#size' => 4,
    );
    $form['label'] = array(
        '#type' => 'fieldset',
        '#title' => t('PDF label setting'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,

    );
    $form['label']['UserName'] = array(
        '#type' => 'textfield',
        '#title' => t('UserName'),
        '#default_value' => variable_get('gls_dc_dfq_UserName', "2080060960"),
    );
    $form['label']['Password'] = array(
        '#type' => 'password',
        '#title' => t('Password'),
        '#default_value' => variable_get('gls_dc_dfq_Password', "API1234"),
    );
    $form['label']['Customerid'] = array(
        '#type' => 'textfield',
        '#title' => t('Customerid'),
        '#default_value' => variable_get('gls_dc_dfq_Customerid', "2080060960"),
    );
    $form['label']['Contactid'] = array(
        '#type' => 'textfield',
        '#title' => t('Contactid'),
        '#default_value' => variable_get('gls_dc_dfq_Contactid', "208a144Uoo"),
    );
    $form['label']['Weight'] = array(
        '#type' => 'textfield',
        '#title' => t('Weight'),
        '#default_value' => variable_get('gls_dc_dfq_Weight', 1),
    );
    $form['info'] = array(
        '#type' => 'fieldset',
        '#title' => t('Information'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );
    $form['info']['lnk'] = array(
        '#markup' => t('If you are using a standard assembly - the menu item to add the list of orders in place convenient for you :') . l('admin/commerce/orders/list_gls', 'admin/commerce/orders/list_gls'),

    );


    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save configuration'),
    );

    return $form;
}

function gls_dc_admin_settings_submit($form, &$form_state)
{
    $values = $form_state['values'];

    if ($form_state['values']['op'] == 'Ok') {
        if (strlen(trim($values['price_op_country'])) == 2 && is_numeric($values['price_op_private']) && is_numeric($values['price_op_companies'])) {
            $count = db_select('gls_dc_price_country', 's')
                ->condition('s.country', trim($values['price_op_country']))
                ->countQuery()
                ->execute()
                ->fetchField();
            if ($count > 0) {
                db_update('gls_dc_price_country')
                    ->fields(array('price_private' => ($values['price_op_private'] * 100), 'price_companies' => ($values['price_op_companies'] * 100)))
                    ->condition('country', trim($values['price_op_country']))
                    ->execute();
                drupal_set_message('Price changed');
            } else {
                db_insert('gls_dc_price_country')
                    ->fields(array(
                        'country' => drupal_strtoupper(trim($values['price_op_country'])),
                        'price_private' => ($values['price_op_private'] * 100),
                        'price_companies' => ($values['price_op_companies'] * 100)
                    ))
                    ->execute();
                drupal_set_message('Price added');
            }
        } elseif (strlen(trim($values['price_op_country'])) == 2 && strlen(trim($values['price_op_private'])) == 0 && strlen(trim($values['price_op_companies'])) == 0) {
            db_delete('gls_dc_price_country')
                ->condition('country', trim($values['price_op_country']))
                ->execute();
            drupal_set_message('Price erased');
        } else {
            drupal_set_message("Not valid input", "error");
        }
    } else {
        variable_set('gls_dc_price_private', $values['price_private']);
        variable_set('gls_dc_price_companies', $values['price_companies']);
        variable_set('gls_dc_gmkey', $values['gmkey']);
        variable_set('gls_dc_gmshow', $values['gmshow']);
        variable_set('gls_dc_gm_width', $values['gm_width']);
        variable_set('gls_dc_gm_height', $values['gm_height']);
        variable_set('gls_dc_dfq_UserName', $values['UserName']);
        if (strlen(trim($values['Password'])) > 0) variable_set('gls_dc_dfq_Password', $values['Password']);
        variable_set('gls_dc_dfq_Customerid', $values['Customerid']);
        variable_set('gls_dc_dfq_Contactid', $values['Contactid']);
        variable_set('gls_dc_dfq_Weight', $values['Weight']);
        drupal_set_message('Settings saved.');
    }
}

function gls_dc_admin_get_table()
{
    $header = array(
        array('data' => t('Country'), 'field' => 'country'),
        array('data' => t('Home delivery (Private) price'), 'field' => 'price_private'),
        array('data' => t('Company delivery (Companies) price'), 'field' => 'price_companies'),
    );

    $prices = db_select('gls_dc_price_country', 's')
        ->fields('s', array('country', 'price_private', 'price_companies'))
        ->extend('PagerDefault')
        ->limit(10)
        ->extend('TableSort')
        ->orderByHeader($header)
        ->execute();

    $rows = array();
    foreach ($prices as $price) {
        $rows[] = array(
            $price->country,
            $price->price_private / 100,
            $price->price_companies / 100
        );
    }

    $output = theme('table', array('header' => $header, 'rows' => $rows));
    $output .= theme('pager');
    return $output;
}